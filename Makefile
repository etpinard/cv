all: fr en

assets:
	$(MAKE) -C assets clean all

fr: cv.sty assets
	$(MAKE) -C fr clean all

en: cv.sty assets
	$(MAKE) -C en clean all

.PHONY: all assets
