# @etpinard/cv

Personal CV (made in LaTeX)

## Dependencies

```
# texlive-full (sorry, but those fonts are so dope)
apt install texlive-full
# or
dnf install textlive-scheme-full

# latexmk
apt install latexmk
# or
dnf install latexmk

# imagemagick
apt install imagemagick
# or
dnf install ImageMagick

# N.B. might need to change the PDF "policy"
# https://stackoverflow.com/a/52717932/4068492

# file watcher (optional, to run `make watch`)
apt install inotify-tools
# or
dnf install inotify-tools

# copy pre-commit hook (optional)
cp -f pre-commit.sh .git/hooks/pre-commit
```

## Targets

```
# changes in en/
pushd en
make clean all
popd

# changes in fr/
pushd fr
make clean all
popd

# changes in cv.sty
make
```
