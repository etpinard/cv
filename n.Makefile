# add repo root to possible input paths to include cv.sty
export TEXINPUTS:=.:..:~/texmf:${TEXINPUTS}

all: cv.pdf cv.png

cv.pdf: cv.tex constants.tex
	latexmk cv.tex -pdf
	latexmk -c
	rm -f cv.dvi

cv.png: cv.pdf
	convert -density 600x600 cv.pdf -quality 90 -flatten cv.png

clean:
	rm -f cv.pdf cv.png

watch:
	while true; do inotifywait -e modify cv.tex; $(MAKE); done

.PHONY: all clean watch
